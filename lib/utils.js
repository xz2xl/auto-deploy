/**
 * @author xiao zhou
 * @lastEditTime 2021-06-02 01:57:51
 * @description
 */

const ora = require('ora')
const chalk = require('chalk')

const isString = o => Object.prototype.toString.call(o).slice(8, -1) === 'String'

module.exports = {
  // 日志信息
  log(message) {
    console.log(message)
  },
  // 成功信息
  succeed(...message) {
    ora().succeed(chalk.greenBright.bold(message))
  },
  // 提示信息
  info(...message) {
    ora().info(chalk.blueBright.bold(message))
  },
  // 警告信息
  warn(...message) {
    ora().warn(chalk.yellowBright.bold(message))
  },
  // 错误信息
  error(...message) {
    ora().fail(chalk.redBright.bold(message))
  },
  // 下划线信息
  underline(message) {
    return chalk.underline.blueBright.bold(message)
  },
  // 格式化时间
  formatTime(time, fmt = 'yyyy-MM-dd hh:mm:ss') {
    const date = isString(time) ? new Date(time.replace(/-/g,'/')) : new Date(time)
    if (isNaN(date.getTime())) return 'NaN'
    const opt = {
      'y+': date.getFullYear().toString(),
      'M+': (date.getMonth() + 1).toString(),
      'd+': date.getDate().toString(),
      'h+': date.getHours().toString(),
      'm+': date.getMinutes().toString(),
      's+': date.getSeconds().toString() //秒
    }
    let ret
    for (let k in opt) {
      ret = new RegExp(`(${k})`).exec(fmt)
      if (ret) {
        fmt = fmt.replace(ret[1], ret[1].length === 1 ? opt[k] : opt[k].padStart(ret[1].length, '0'))
      }
    }
    return fmt
  }
}

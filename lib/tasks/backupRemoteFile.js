/**
 * @author xiao zhou
 * @lastEditTime 2021-06-10 15:29:47
 * @description
 */

const { log, succeed, error, underline, formatTime } = require('../utils')

module.exports = async (ssh, config, index) => {
  try {
    const { webDir, bakDir } = config
    const dirName = webDir.split('/')[webDir.split('/').length - 1]
    const zipFileName = `${dirName}_${formatTime(new Date(), 'yyyy-MM-dd_hh:mm:ss')}.zip`
    log(`(${index}) 备份远程文件 ${underline(webDir)}`)
    await ssh.execCommand(`[ ! -d ${bakDir} ] && mkdir ${bakDir}`)
    await ssh.execCommand(`zip -q -r ${bakDir}/${zipFileName} ${webDir}`)
    succeed(`备份成功 备份至 ${underline(`${bakDir}/${zipFileName}`)}`)
  } catch (e) {
    error(e)
    // eslint-disable-next-line no-undef
    process.exit(1)
  }
}

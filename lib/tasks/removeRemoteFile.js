/**
 * @author xiao zhou
 * @lastEditTime 2021-06-10 15:30:24
 * @description
 */

const { log, succeed, error, underline } = require('../utils')

module.exports = async (ssh, config, index) => {
  try {
    const { webDir } = config
    log(`(${index}) 删除远程旧文件 ${underline(webDir)}`)
    await ssh.execCommand(`rm -rf ${webDir} && mkdir ${webDir}`)
    succeed('删除成功')
  } catch (e) {
    error(e)
    // eslint-disable-next-line no-undef
    process.exit(1)
  }
}

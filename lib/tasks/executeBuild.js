/**
 * @author xiao zhou
 * @lastEditTime 2021-06-10 15:30:12
 * @description
 */

const ora = require('ora')
const childProcess = require('child_process')
const { log, succeed, error } = require('../utils')

const maxBuffer = 5000 * 1024

module.exports = async (config, index) => {
  try {
    const { script } = config
    log(`(${index}) 本地打包： ${script}`)
    const spinner = ora('正在打包中\n')
    spinner.start()
    await new Promise((resolve, reject) => {
      // eslint-disable-next-line no-undef
      childProcess.exec(script, { 'cwd': process.cwd(), 'maxBuffer': maxBuffer }, e => {
        spinner.stop()
        if (e === null) {
          succeed('打包成功')
          resolve()
        } else {
          reject(e.message)
        }
      }
      )
    })
  } catch (e) {
    error('打包失败')
    error(e)
    // eslint-disable-next-line no-undef
    process.exit(1)
  }
}

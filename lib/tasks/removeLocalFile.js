/**
 * @author xiao zhou
 * @lastEditTime 2021-06-02 01:55:44
 * @description 删除本地zip包
 */

const fse = require('fs-extra')
const { log, succeed, underline } = require('../utils')

module.exports = async (config, index) => {
  // eslint-disable-next-line no-undef
  const localPath = `${process.cwd()}/${config.distPath}`
  log(`(${index}) 删除本地打包目录 ${underline(localPath)}`)
  fse.removeSync(localPath)
  fse.removeSync(`${localPath}.zip`)
  succeed('删除本地打包目录成功')
}

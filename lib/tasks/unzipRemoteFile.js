/**
 * @author xiao zhou
 * @lastEditTime 2021-06-02 01:55:52
 * @description
 */

const { log, succeed, error, underline } = require('../utils')

module.exports = async (ssh, config, index) => {
  try {
    const { webDir } = config
    const remoteFileName = `${webDir}.zip`
    log(`(${index}) 解压远程文件 ${underline(remoteFileName)}`)
    await ssh.execCommand(`unzip -o ${remoteFileName} -d ${webDir} && rm -rf ${remoteFileName}`, { 'cwd': webDir })
    succeed('zip包解压成功')
  } catch (err) {
    error(`zip包解压失败 ${err}`)
    // eslint-disable-next-line no-undef
    process.exit(1)
  }
}

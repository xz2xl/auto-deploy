/**
 * @author xiao zhou
 * @lastEditTime 2021-06-10 15:30:38
 * @description
 */

const ora = require('ora')
const { log, succeed, error, underline } = require('../utils')

module.exports = async (ssh, config, index) => {
  try {
    const { webDir, distPath } = config
    // eslint-disable-next-line no-undef
    const localFileName = `${process.cwd()}/${distPath}.zip`
    const remoteFileName = `${webDir}.zip`
    log(`(${index}) 上传打包zip至目录 ${underline(remoteFileName)}`)
    const spinner = ora('正在上传中\n')
    spinner.start()
    await ssh.execCommand(`[ ! -d ${webDir} ] && mkdir ${webDir}`)
    await ssh.putFile(localFileName, remoteFileName, null, {
      'concurrency': 1
    })
    spinner.stop()
    succeed('zip包上传成功')
  } catch (err) {
    error(`zip包上传失败: ${err}`)
    // eslint-disable-next-line no-undef
    process.exit(1)
  }
}

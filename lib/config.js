/**
 * @author xiao zhou
 * @lastEditTime 2021-06-09 15:30:36
 * @description
 */


const fse = require('fs-extra')
const os = require('os')

const envList = ['dev', 'test', 'prod']

// eslint-disable-next-line no-undef
const projectPkg = fse.pathExistsSync(`${process.cwd()}/package.json`) ? require(`${process.cwd()}/package.json`) : {}

module.exports = {
  'packageInfo': require('../package.json'),
  'projectPkg': projectPkg,
  // eslint-disable-next-line no-undef
  'deployConfigPath': `${process.cwd()}/deploy.config.js`,
  'baseConfig': [
    {
      'type': 'input',
      'name': 'projectName',
      'message': '请输入项目名称',
      'default': projectPkg.name ?? ''
    },
    {
      'type': 'input',
      'name': 'privateKey',
      'message': '请输入本地私钥地址',
      'default': `${os.homedir()}/.ssh/id_rsa`
    },
    {
      'type': 'password',
      'name': 'passphrase',
      'message': '请输入本地私钥密码',
      'default': ''
    },
    {
      'type': 'checkbox',
      'name': 'deployEnvList',
      'message': '请选择需要部署的环境',
      'choices': envList.map((env, index) => ({
        'name': env,
        'checked': index === 0
      }))
    }
  ]
}

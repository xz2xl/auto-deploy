## @xz2xl/auto-deploy

<hr>

前端一键自动化部署工具，支持开发、测试、生产多环境配置，支持单环境部署和多环境同时部署。配置好后即可一键自动完成打包、部署。


国内文档： [gitee](https://gitee.com/xz2xl/auto-deploy)


> 推荐：
> node >= 12.1.0

### 安装
全局安装(推荐)

``` bash
npm install -g @xz2xl/auto-deploy
```

本地安装

``` bash
npm install @xz2xl/auto-deploy --save-dev
```

> 注意：采用本地安装方法的话一下所有命令前面要加上 `npx`， 例如： npx @xz2xl/auto-deploy --version

查看版本

``` bash
@xz2xl/auto-deploy --version
# @xz2xl/auto-deploy -v
```

### 使用

以下以全局安装且在项目根目录下为例

1、 初始化项目

``` bash
@xz2xl/auto-deploy init
# 或者使用简写 @xz2xl/auto-deploy i
```

2、 手动创建或修改配置文件
不使用上面的初始化命令的也可以手动创建配置文件。在项目根目录下手动创建 `deploy.config.js` 文件，复制以下代码按情况修改即可。
> 注意：server中的各环境配置换成自己的配置

``` javascript
module.exports = {
  'projectName': 'vue-antd-admin',          // 项目名称
  'privateKey': '',                         // 本地私钥地址。 Mac默认：/Users/[用户名]/.ssh/id_rsa
  'passphrase': '',                         // 本地私钥密码，没有则不填，默认为空
  'server': {                               // 部署服务器配置
    'dev': {                                // 开发环境
      'name': '开发环境',                    // 服务器配置名称
      'script': 'npm run build:dev',        // 打包命令
      'host': '192.168.1.3',                // 服务器地址
      'port': 22,                           // 服务器端口号
      'username': 'xiaozhou',               // 服务器登录名
      'password': '',                       // 服务器登录明码，
      'distPath': 'dist',                   // 打包后文件路径，默认：dist
      'webDir': '/Users/xiaozhou/web',      // 服务器部署路径，不可为空或者'/'
      'bakDir': '/Users/xiaozhou/back',     // 服务器文件备份路径
      'isRemoveRemoteFile': true,           // 是否删除远程文件，默认：true
      'isRemoveLocalFile': true             // 是否删除本地打包文件，默认：true
    },
    'test': {                               // 测试环境
      'name': '测试环境',                    // 服务器配置名称
      'script': 'npm run build:test',       // 打包命令
      'host': '192.168.1.3',                // 服务器地址
      'port': 22,                           // 服务器端口号
      'username': 'xiaozhou',               // 服务器登录名
      'password': '',                       // 服务器登录明码，
      'distPath': 'dist',                   // 打包后文件路径，默认：dist
      'webDir': '/Users/xiaozhou/web',      // 服务器部署路径，不可为空或者'/'
      'bakDir': '/Users/xiaozhou/back',     // 服务器文件备份路径
      'isRemoveRemoteFile': true,           // 是否删除远程文件，默认：true
      'isRemoveLocalFile': true             // 是否删除本地打包文件，默认：true
    },
    'prod': {                               // 生产环境
      'name': '生产环境',                    // 服务器配置名称
      'script': 'npm run build:prod',       // 打包命令
      'host': '192.168.1.3',                // 服务器地址
      'port': 22,                           // 服务器端口号
      'username': 'xiaozhou',               // 服务器登录名
      'password': '',                       // 服务器登录明码，
      'distPath': 'dist',                   // 打包后文件路径，默认：dist
      'webDir': '/Users/xiaozhou/web',      // 服务器部署路径，不可为空或者'/'
      'bakDir': '/Users/xiaozhou/back',     // 服务器文件备份路径
      'isRemoveRemoteFile': true,           // 是否删除远程文件，默认：true
      'isRemoveLocalFile': true             // 是否删除本地打包文件，默认：true
    }
  }
}
```

3、 部署
- 单环境部署 `--mode 环境对象`

``` bash
@xz2xl/auto-deploy deploy --mode dev
# OR
@xz2xl/auto-deploy deploy --mode test
# OR
@xz2xl/auto-deploy deploy --mode prod
```

- 多环境部署，发布到 `deploy.config.js` 文件中的 `server` 字段下定义的所有环境中

``` bash
@xz2xl/auto-deploy deploy
# OR
@xz2xl/auto-deploy d
```

输入 Y 或按下 Enter 键确认后即可开始自动部署。

> 注：可以在命令后添加 `--no-build` 跳过本地打包过程 

### 本地安装扩展
运行 `@xz2xl/auto-deploy init` 后将会自动添加对应的代码到项目根目录下的 `package.json` 文件中 `scripts` 脚本中

``` json
"scripts": {
  // ...
  "deploy": "@xz2xl/auto-deploy deploy",
  "deploy:dev": "@xz2xl/auto-deploy deploy --mode dev",
  "deploy:test": "@xz2xl/auto-deploy deploy --mode test",
  "deploy:prod": "@xz2xl/auto-deploy deploy --mode prod"
}
```

然后使用下面代码也可以完成部署操作

```  bash
npm run deploy:dev
# OR 
npm run deploy:test
# OR 
npm run deploy:prod
```

### 自定义环境
本项目仅内置 `dev`、`test`、`prod` 三种环境，若想添加自定义环境配置，在 `deploy.config.js` 文件中的 `server` 字段下按以下格式添加代码，同时在 `package.json` 文件中 `scripts` 脚本中添加 `"deploy:customEnv": "@xz2xl/auto-deploy deploy --mode customEnv"` 即可。

例如：

``` javascript
module.exports = {
  // ...
  'server': {                               // 部署服务器配置
    // ...
    'customEnv': {                          // 自定义环境
      'name': '自定义环境',                    // 服务器配置名称
      'script': 'customEnv 打包命令',        // 打包命令
      'host': '192.168.1.3',                // 服务器地址
      'port': 22,                           // 服务器端口号
      'username': 'xiaozhou',               // 服务器登录名
      'password': '',                       // 服务器登录明码，
      'distPath': 'dist',                   // 打包后文件路径，默认：dist
      'webDir': '/Users/xiaozhou/web',      // 服务器部署路径，不可为空或者'/'
      'bakDir': '/Users/xiaozhou/back',     // 服务器文件备份路径
      'isRemoveRemoteFile': true,           // 是否删除远程文件，默认：true
      'isRemoveLocalFile': true             // 是否删除本地打包文件，默认：true
    }
  }
}
```

### 安全性

1、 服务器的密码放上去是否不太安全？

> 密码可以不填，再把 `privateKey` 字段值修改成 ''，部署的时候会提示让你输入密码
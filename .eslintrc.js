module.exports = {
  'env': {
    'browser': true,
    'commonjs': true,
    'es2021': true
  },
  'extends': 'eslint:recommended',
  'parserOptions': {
    'ecmaVersion': 12,
    'sourceType': 'module'
  },
  'rules': {
    'linebreak-style': [ 'error','unix'],
    //关闭“禁用console”规则
    'no-console': 'off',
    //缩进不规范警告，要求缩进为2个空格，默认值为2个空格
    'indent': ['warn', 2, {
      //设置为1时强制switch语句中case的缩进为2个空格
      'SwitchCase': 1,
      'VariableDeclarator': 2
    }],
    //定义字符串不规范错误，要求尽可能地使用单引号
    'quotes': [2, 'single', { 'allowTemplateLiterals': true }], // 引号风格
    // 要求使用 === 和 !== (eqeqeq)
    'eqeqeq': ['error', 'always'],
    // 要求或禁止文件末尾保留一行空行 (eol-last)
    'eol-last': ['error', 'always'],
    // 禁止使用拖尾逗号
    'comma-dangle': ['error', 'never'],
    // 禁用行尾空白
    'no-trailing-spaces': 'error',
    // 禁止在语句末尾使用分号
    'semi': ['error', 'never'],
    // 要求箭头函数的箭头之前或之后有空格
    'arrow-spacing': ['error', { 'before': true, 'after': true }],
    'camelcase': ['error', { 'properties': 'never' }],
    'keyword-spacing': 2, // 关键字前后的空格
    'key-spacing': [2, { 'beforeColon': false, 'afterColon': true }], // 对象字面量中冒号的前后空格
    'no-extra-parens': 'error', // 不允许出现不必要的圆括号
    'no-extra-semi': 2, // 不允许出现不必要的分号
    'no-multi-spaces': 2, // 不允许出现多余的空格
    'no-spaced-func': 2, // 函数调用时 函数名与()之间不能有空格
    'no-multiple-empty-lines': [2, { 'max': 2 }], // 空行最多不能超过两行
    'no-unused-vars': [2, { 'vars': 'all', 'args': 'none' }], // 不允许有声明后未使用的变量或者参数
    'no-mixed-spaces-and-tabs': ['error', 'smart-tabs'], // 禁止使用 空格 和 tab 混合缩进
    'no-use-before-define': [2, 'nofunc'], // 不允许在未定义之前就使用变量
    'no-var': 0, // 使用let和const代替var
    'new-cap': [2, { 'newIsCap': true, 'capIsNew': false }], // 构造函数名字首字母要大写
    'newline-after-var': 0, // 变量声明后必须空一行
    'object-curly-spacing': ['error', 'always'], // 允许在大括号之间留出空格
    'quote-props': 2, // 对象中的属性名是否需要用引号引起来
    'space-before-blocks': [2, 'always'], // 块前的空格
    'space-infix-ops': [2, { 'int32Hint': true }], // 操作符周围的空格
    'space-unary-ops': [2, { 'words': true, 'nonwords': false }], // 一元运算符前后加空格
    'space-before-function-paren': ['error', {
      'anonymous': 'never',
      'named': 'never',
      'asyncArrow': 'always'
    }]
  }
}
